var model = require("../models/viewModel");

exports.index_get = function (request, response) {
    return model.view({
        renderPage: "index",
        renderData: {
            categories: [
                { name: "berko", id: 1 },
                { name: "gunero", id: 2 }
            ]
        }
    });
}

exports.index_post = function (request, response) {

}