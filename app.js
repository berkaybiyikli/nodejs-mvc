var express = require("express");
var fs = require('fs');
var viewModel = require("./models/viewModel");
const controllerFolder = "./controllers/";

var app = express();
app.set('view engine', 'ejs');

var returnData = viewModel.data;

app.all("/:controller/:action/:id?", (request, response) => {

    var controllerName = request.params.controller + ".controller.js",
        actionName = request.params.action.toLowerCase() + "_" + request.method.toLowerCase();

    if (fs.existsSync(controllerFolder + controllerName)) {

        var controllerModule = require(controllerFolder + controllerName);

        Object.keys(controllerModule).forEach(function (functionName) {
            if (functionName.toLowerCase() === actionName) {
                if (controllerModule[functionName]) {
                    returnData = controllerModule[functionName](request, response);
                } else {
                    returnData = {
                        isRedirect: true,
                        redirectUrl: "/404",
                        resultMessage: "Action Bulunamadı",
                        statusCode: 404
                    }
                }
                return;
            }
        });

    } else {
        returnData = {
            isRedirect: true,
            redirectUrl: "/404",
            resultMessage: "Controller Bulunamadı",
            statusCode: 404
        }
    }

    if (returnData.isRedirect) {
        response.redirect(returnData.redirectUrl);
    } else {
        response.render(returnData.renderPage, returnData.renderData);
    }
});


app.listen(2025, () => {
    console.log("listen 2025 port");
});